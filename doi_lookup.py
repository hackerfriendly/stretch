#!/usr/bin/env python3
import argparse
import sys
import json
import logging

from logging import debug, critical
from logging import warning as warn

from datetime import (datetime, timezone)

import networkx as nx
from networkx.readwrite import json_graph
from elasticsearch import Elasticsearch
from habanero import (Crossref, cn)

class doi_xref:
    def __init__(self, server, port, index):
        self.es = Elasticsearch([f"{server}:{port}"])
        self.index = index

        self.G = nx.DiGraph()

    @staticmethod
    def prepare_doc(reply):
        ''' Pull relevant fields from Crossref into Elasticsearch format '''
        doc = {}
        doc["@timestamp"] = str(datetime.now(timezone.utc).astimezone().isoformat())

        item = reply['message']
        doc['created'] = item['created']['date-time']

        if "published-print" in item and "date-parts" in item["published-print"]:
            doc["year"] = item["published-print"]["date-parts"][0][0]
        else:
            doc["year"] = item["created"]["date-parts"][0][0]

        for k in (
            'reference-count',
            'publisher',
            'issue',
            'DOI',
            'page',
            'is-referenced-by-count',
            'volume',
            'author',
            'container-title',
            'references-count',
            'URL',
            'subject',
            'language',
        ):
            if k in item:
                doc[k] = item[k]

        # First title only
        if isinstance(item['title'], list):
            doc['title'] = item['title'][0]
        else:
            doc['title'] = item['title']

        debug(f"prepare_doc: {doc}")

        return doc

    def lookup(self, dois, refresh=False):
        '''
        Look up the DOI and any references in Elasticsearch.
        If not found, pull data from Crossref and update ES.
        '''
        if not isinstance(dois, list):
            dois = [dois]

        docs = []
        for doi in dois:
            res = self.es.search(index=self.index, body={"query": {"term": {"DOI.keyword": doi}}})
            _id = None
            if refresh and res['hits']['total']['value']:
                _id = res['hits']['hits'][0]['_id']
                res = {'hits': { 'total': { 'value': 0 } } }

            debug(f"res: {json.dumps(res)}")

            if res['hits']['total']['value']:
                doc = res['hits']['hits'][0]['_source']
                doc['db'] = 'Elasticsearch'
                _id = res['hits']['hits'][0]['_id']
            else:
                cr = Crossref(mailto="crossref@interimresearch.io")
                try:
                    doc = self.prepare_doc(cr.works(ids = doi))
                    doc['db'] = 'Crossref'
                except Exception as e: # requests HTTPError
                    debug(str(e))
                    continue
                if _id:
                    self.es.update(index=self.index, id=_id, body={"doc": doc})
                else:
                    _id = self.es.index(index=self.index,  body=doc)["_id"]
                debug(f"_id: {_id}")

            # some titles have embedded \n
            if "title" in doc:
                doc["title"].replace("\n", " ")

            if "references" not in doc:
                citations = set()
                try:
                    reply = json.loads(cn.content_negotiation(ids = doi, format = "citeproc-json"))
                    debug(f"cn reply: {reply}")

                    for ref in reply["reference"]:
                        if "DOI" in ref:
                            citations.add(ref["DOI"])
                except Exception: # requests HTTPError, KeyError
                    pass

                doc["references"] = [c for c in citations]
                debug(f"references: {doc['references']}")

                # new references?
                if doc["references"] and not refresh:
                    self.es.update(
                        index=self.index,
                        id=_id,
                        body={"doc": {"references": doc["references"]}}
                    )
            docs.append(doc)

        return docs

    def get_node(self, doc):
        if "year" not in doc:
            doc = self.lookup(doc["DOI"], refresh=True)[0]

        try:
            first_author = f"{doc['author'][0]['given']} {doc['author'][0]['family']}"
            last_author =  f"{doc['author'][-1]['given']} {doc['author'][-1]['family']}"
        except KeyError:
            first_author = str(doc['author'][0])
            last_author = str(doc['author'][-1])

        if first_author == last_author:
            last_author == ""

        primary_subject = ""
        secondary_subject = ""
        subjects = doc.get("subject", [])
        if subjects:
            primary_subject = subjects[0]
            if len(subjects) > 1:
                secondary_subject = subjects[1]

        if "language" in doc:
            the_language = doc["language"]
        else:
            the_language = "unknown"

        return [(
            doc["DOI"],
            {
                "title": doc["title"],
                "is-referenced-by-count": doc.get("is-referenced-by-count", 0),
                "first-author": first_author,
                "last-author": last_author,
                "author-count": len(doc['author']),
                "primary-subject": primary_subject,
                "secondary-subject": secondary_subject,
                "published": doc["created"],
                "year": doc["year"],
                "language": the_language,
            }
        )]

    def add_nodes(self, doc, extra=None):
        '''
        Add nodes to the graph
        '''
        doi = doc['DOI'].lower()
        if doi in self.G.nodes:
            return

        the_node = self.get_node(doc)

        if "language" in the_node[0][1] and the_node[0][1]["language"] not in ["en", "unknown"]:
            return

        self.G.add_nodes_from(the_node, name=doc["title"], type="paper", extra=extra)

        for order in ["first-author", "last-author"]:
            author = the_node[0][1][order]
            if author not in self.G.nodes:
                self.G.add_nodes_from([(author, {})], type="author")
            self.G.add_edge(author, doi, authorship=order)

        subject = the_node[0][1]["primary-subject"]
        if subject:
            if subject not in self.G.nodes:
                self.G.add_nodes_from([(subject, {})], type="subject")
            self.G.add_edge(doi, subject)

    def add_edges(self, src_node, dest_node, doc):
        '''
        Add weighted edges to the graph
        '''
        self.G.add_weighted_edges_from([(src_node, dest_node, doc["is-referenced-by-count"])])

    def depth_search(self, start_doi, max_depth, max_docs, outfile, refresh=False):
        '''
        Look up all the references in refs, up to max_depth links out.
        Stop after max_docs have been retrieved.

        Prints a Cytoscape-ready JSON doc of every doi found.
        '''

        doc = self.lookup(start_doi, refresh=refresh)[0]
        if not doc["references"]:
            critical(f"no references in {start_doi}")
            return None
        self.add_nodes(doc, extra="seed")

        todo = [(start_doi.lower(), doc["references"])]

        for depth in range(max_depth):
            while todo and (len(self.G.nodes) < max_docs):
                (doi, refs) = todo.pop(-1)
                warn(f"{doi} -> {len(refs)} references, {len(self.G.nodes)} total so far")

                doc = self.lookup(doi, refresh=refresh)[0]
                if not doc or 'author' not in doc:
                    critical(f'no author for doi {doi}')
                    continue
                if not refs:
                    critical(f'no refs for doi {doi}')
                    continue
                self.add_nodes(doc)

                for subdoc in self.lookup(refs, refresh=refresh):
                    if len(self.G.nodes) >= max_docs:
                        break

                    ref = subdoc["DOI"].lower()

                    if ref in self.G.nodes:
                        warn(f"already seen {ref}")
                        continue

                    if not subdoc or 'author' not in subdoc:
                        critical(f'no author for ref {ref}')
                        continue

                    self.add_nodes(subdoc)
                    self.add_edges(ref, doi, doc)

                    if "references" not in subdoc:
                        critical(f'no refs in ref {ref}')
                        continue

                    todo.append((ref, subdoc["references"]))

            if len(self.G.nodes) >= max_docs:
                warn(f"Stopping at {max_docs} docs")
                break

            critical(f"depth == {depth + 1}")

        print(json.dumps(json_graph.cytoscape_data(self.G)), file=outfile)

def parse_args(clargs):
    ''' Argparse options '''
    parser = argparse.ArgumentParser(description="Look up a DOI. Try the Elastic cache first, then fall back on Crossref.",
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("doi", type=str, help="Look up this DOI")
    parser.add_argument("-o", "--out", default="/dev/stdout", help="Output file (default: STDOUT)")
    parser.add_argument("--depth", type=int, default=0, help="Expand the search to this many links away from starting document")
    parser.add_argument("--max", type=int, default=100, help="Stop after collecting this many documents")
    parser.add_argument("--server", default="aramaki.s9", help="Elasticsearch server (default: %(default)s)")
    parser.add_argument("--port", default="9200", help="Elasticsearch port (default: %(default)s)")
    parser.add_argument("--index", default=f"crossref-2021-01", help="Elasticsearch index (default: %(default)s)")
    parser.add_argument("-r", "--refresh", action="store_true", help="Refresh Elasticsearch cache from Crossref")
    parser.add_argument("--debug", action="store_true", help="Debug mode")

    args = parser.parse_args(clargs)

    logging.basicConfig(
        stream=sys.stderr,
        level=logging.DEBUG if args.debug else logging.WARNING,
        format='%(asctime)s : %(message)s' if args.debug else '%(message)s'
    )

    return args

def main(clargs):
    ''' Just Do It '''
    args = parse_args(clargs)

    xref = doi_xref(server=args.server, port=args.port, index=args.index)

    with open(args.out, "w") as outfile:
        if args.depth:
            xref.depth_search(args.doi, args.depth, args.max, outfile, refresh=args.refresh)
        else:
            the_doc = xref.lookup(args.doi, refresh=args.refresh)
            if the_doc:
                print(json.dumps(the_doc[0], indent=2), file=outfile)

if __name__ == "__main__":
    main(sys.argv[1:])
